(function ($) {

    $(document).ready(function(){
        $('.c-home-slide__content-wrap').ready(function () {
          setTimeout(function () {
            $('.c-home-slide__content-wrap').addClass('u-angle-in--banner');
          }, 500)
        });

        $('.s-hero__bg').ready(function () {
          setTimeout(function () {
            $('.s-hero__bg').addClass('u-angle-in--banner');
          }, 500)
        });

        $('.s-about').find('.c-image-text-box__image').each(function() {
            if ($(window).width() < 768) {
                $(this).find('img').attr('data-aos', 'u-angle-in--right').attr('data-aos-duration', '100');
            } else {
                $(this).find('img').attr('data-aos', 'u-angle-in--left').attr('data-aos-duration', '100');
            }
        });

        AOS.init();
    });

})(jQuery);
