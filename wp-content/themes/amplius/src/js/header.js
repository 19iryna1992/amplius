// const { on } = require("gulp");

(function ($) {

  $(document).ready(function () {

    $(document).on('click','.JS-menu-toggle',function(){
      $(this).toggleClass('open');
      $('body').toggleClass('u-overflow');
      $('.o-header__wrap').toggleClass('open');
      $('.JS-menu').toggle();
    });

    var menuLink = $('.JS-menu').find('.menu-item').children('a');

    menuLink.on('click', function(){
      viewportwidth = $(window).width();
      if (viewportwidth<1200) {
        $('.JS-menu').toggle();
      }

      $(this).removeClass('open');
      $('body').removeClass('u-overflow');
      $('.o-header__wrap').removeClass('open');
      
    });

    window.onresize = function(event) {
      viewportwidth = $(window).width();
      if (viewportwidth>=1200) {
        $('body').removeClass('u-overflow');
        $('.o-header__wrap').removeClass('open');
      }
    };

    var $header = $('.JS-header'),
      $scroll = $(window).scrollTop();

    if ($scroll >= 50) $header.addClass('fixed');
    else $header.removeClass('fixed');

    $(window).scroll(function(){
      var header = $('.JS-header'),
          scroll = $(window).scrollTop();
    
      if (scroll >= 50) header.addClass('fixed');
      else header.removeClass('fixed');
    });
  });

})(jQuery);
