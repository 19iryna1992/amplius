(function ($) {

  $(document).ready(function () {
    $('.s-our-teams').find('.c-team').each(function () {
      var $team = $(this);

      $($team).find('a').hover(
        function() {
          $('.s-our-teams').find('.c-team').not($team).removeClass('u-opacity-out').addClass('u-opacity-in');
          $(this).find('img').removeClass('u-angle-out').addClass('u-angle-in');
        },
        function () {
          $('.s-our-teams').find('.c-team').not($team).removeClass('u-opacity-in').addClass('u-opacity-out');
          $(this).find('img').removeClass('u-angle-in').addClass('u-angle-out');
        })
    })
  });

})(jQuery);
