(function ($) {

    $('body').on("click", '.JS--load-more', function (e) {
        e.preventDefault();
        setTimeout(function () {
            newsLoadMore();

        }, 100);

    });

    function newsLoadMore() {
        var maxPages = $('.s-posts').data('pages');
        $.ajax({
            url: my_ajax_object.ajax_url,
            data: {
                action: "load_more_posts",
                page: my_ajax_object.page,
            },
            type: 'POST',
            beforeSend: function () {

            },

            success: function (data) {
                $('.JS--post-container').append(data).fadeIn(250);
                my_ajax_object.page++;
                if(my_ajax_object.page == maxPages){
                   $('.JS--load--container').remove();
                }
            },

            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    }

})(jQuery);





