<?php

add_action('wp_ajax_load_more_posts', 'load_more_posts');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');

function load_more_posts()
{
    $args = [
        'post_type' => 'post',
        'posts_per_page' => 5,
        'paged' => $_POST['page'] + 1,
    ];
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post(); ?>

                <?php get_template_part('template-parts/components/excerpt-post'); ?>

        <?php endwhile;
        wp_reset_postdata();
    endif;

    wp_die();

}
