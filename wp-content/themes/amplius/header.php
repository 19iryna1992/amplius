<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amp
 */
$current_user = wp_get_current_user();
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header class="o-header JS-header"  data-aos="fade-zoom-in"
                                        data-aos-easing="ease-in-back"
                                        data-aos-delay="300"
                                        data-aos-offset="0">
        <div class="o-header__wrap">
            <div class="container">
                <div class="row justify-content-between justify-content-xl-center align-items-center">
                    <div class="col-10 col-md-auto">
                        <div class="o-header__logo">
                            <?php the_custom_logo();?>
                        </div>
                    </div>
                    <div class="col-2 d-xl-none">
                        <div class="c-menu-primary__btn JS-menu-toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="c-menu-primary JS-menu">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'header-menu-primary',
                                'menu_class' => 'c-primary-menu__list',
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="content" class="site-content">