<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package amp
 */

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/sections/hero'); ?>

        <?php get_template_part('template-parts/components/intro'); ?>

        <?php get_template_part('template-parts/sections/about'); ?>

        <?php get_template_part('template-parts/sections/large-text-cta'); ?>

    </main>

<?php get_footer(); ?>