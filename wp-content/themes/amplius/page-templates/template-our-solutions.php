<?php /* Template Name: Our Solutions Template */

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/sections/hero'); ?>

        <?php get_template_part('template-parts/sections/intro-text-and-quote'); ?>

        <?php get_template_part('template-parts/sections/list'); ?>

        <?php get_template_part('template-parts/sections/large-text-cta'); ?>

    </main>


<?php get_footer(); ?>