<?php /* Template Name: News Insights Template */


get_header();


$posts_args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 5,
);

$posts = new WP_Query($posts_args);

//$count_posts = wp_count_posts('post')->publish;

?>


    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php get_template_part('template-parts/sections/hero'); ?>

            <?php if ($posts->have_posts()) : ?>
                <section class="s-posts" data-pages="<?= $posts->max_num_pages; ?>">
                    <div class="c-bg-logo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="760" height="775" viewBox="0 0 760 775">
                            <g fill="#F6FBFE" fill-rule="evenodd">
                                <path d="M379.503 164.822l70.067 120.876H309.43l70.073-120.876zm-47.766 241.762l-22.307-38.47h140.14l-22.286 38.444 47.76 82.437L569 326.905 379.503 0 190 326.906 283.961 489l47.776-82.416z"></path>
                                <path d="M666.201 164L570.793 164 664.602 326.654 475.4 654.703 380.013 489.307 284.61 654.719 95.398 326.654 189.207 164 93.809 164 0 326.654 258.585 775 310.635 775 380.003 654.729 449.365 775 501.415 775 760 326.648 666.201 164"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="container"
                         data-aos="fade-up"
                         data-aos-duration="500"
                         data-aos-easing="ease-in-back">
                        <div class="row">
                            <div class="col-12 col-lg-9">
                                <div class="JS--post-container">
                                    <?php while ($posts->have_posts()) :
                                        $posts->the_post() ?>
                                        <?php get_template_part('template-parts/components/excerpt-post'); ?>

                                    <?php endwhile;
                                    wp_reset_postdata(); ?>
                                </div>
                            </div>
                            <?php if ($posts->max_num_pages > 1): ?>
                                <div class="col-12">
                                    <div class="JS--load--container">
                                        <div class="c-load-more">
                                            <a class="c-button--primary JS--load-more"
                                               href="javascript:void(0);"><?php _e('Load More', 'amp'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        </main>
    </div>

<?php get_footer(); ?>