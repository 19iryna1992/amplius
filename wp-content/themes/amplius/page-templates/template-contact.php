<?php /* Template Name: Contact Template */

get_header(); ?>


<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/sections/hero'); ?>

    <?php get_template_part('template-parts/sections/contact-page'); ?>

    <?php get_template_part('template-parts/sections/map');?>

</main>


<?php get_footer(); ?>
