<?php /* Template Name: Home Page Template */

get_header(); ?>

<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/sections/hero-slider'); ?>

    <?php get_template_part('template-parts/sections/advantages-tabs'); ?>

    <?php get_template_part('template-parts/sections/contact-us'); ?>

</main>

<?php get_footer(); ?>

