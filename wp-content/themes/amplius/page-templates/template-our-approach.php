<?php /* Template Name: Our Approach Template */

get_header(); ?>


<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/sections/hero'); ?>

    <?php get_template_part('template-parts/sections/our-approach'); ?>

</main>


<?php get_footer(); ?>
