<?php /* Template Name: Our Resource Partners */

get_header(); ?>

<main id="main" role="main" tabindex="-1">

    <?php get_template_part('template-parts/sections/hero'); ?>


    <?php get_template_part('template-parts/sections/partners'); ?>

</main>


<?php get_footer(); ?>
