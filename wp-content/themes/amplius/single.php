<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package amp
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php while (have_posts()) : the_post() ?>

            <?php get_template_part('template-parts/sections/hero'); ?>
            <div class="container"  
                data-aos="fade-up"
                data-aos-duration="500">
                <div class="row">
                    <div class="col-12">
                        <div class="s-single-post__return-btn">
                            <a class="c-button--link"
                               href="<?php echo get_post_type_archive_link('post'); ?>"><span><</span> <?php _e('Back to news & insights', 'amp'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <section class="s-single-post">
                <div class="container"
                data-aos="fade-up"
                data-aos-duration="500">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-7">
                            <div class="c-single-post">
                                <h3 class="c-single-post__title u-navy"><?= the_title() ?></h3>

                                <?php get_template_part('template-parts/components/wysiwyg'); ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-5">
                            <div class="c-single-post__thumbnail"
                            data-aos="u-angle-in--left"
                            data-aos-duration="100">
                                <?php the_post_thumbnail($size = 'size_478_672', $attr = '') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endwhile ?>
    </main>
</div>

<?php get_footer(); ?>
