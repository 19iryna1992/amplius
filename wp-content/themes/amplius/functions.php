<?php

/**
 * amp functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package amp
 */

require_once __DIR__ . '/inc/functions.php';
require_once __DIR__ . '/post-type/post-types.php';


/*------------------------------------*\
  Theme General Settings
\*------------------------------------*/

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init()
{
    if (function_exists('acf_add_options_page')) {

        acf_add_options_page(array(
            'page_title' => 'Theme General Settings',
            'menu_title' => 'Theme Settings',
            'menu_slug' => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ));

        acf_add_options_sub_page(array(
            'page_title' => 'Theme Footer Settings',
            'menu_title' => 'Footer',
            'parent_slug' => 'theme-general-settings',
        ));

    }
}


/*------------------------------------*\
	Theme Custom Settings
\*------------------------------------*/


if (!function_exists('amp_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function amp_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Chiquita, use a find and replace
         * to change 'chiquita' to the name of your theme in all the template files.
         */
        load_theme_textdomain('amp', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');


        // image sizes
        add_image_size('size_478_672', 478, 672, true);
        add_image_size('size_344_246', 344, 246, true);
        add_image_size('size_480_343', 480, 343, true);
        add_image_size('size_544_528', 544, 528, true);
        add_image_size('size_544_400', 544, 400, true);

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'header-menu-primary' => esc_html__('Primary', 'amp'),
            //'main-mobile-menu' => esc_html__('Mobile main menu', 'amp'),
            'footer-top-menu' => esc_html__('Footer top menu', 'amp'),
            'footer-bottom-menu' => esc_html__('Footer bottom menu', 'amp'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('amp_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 50,
            'width' => 196,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'amp_setup');


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function amp_widgets_init()
{

    register_sidebar(array(
        'name' => esc_html__('Footer Column 1', 'amp'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'amp'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'amp_widgets_init');


/*------------------------------------*\
	Enqueue scripts and styles.
\*------------------------------------*/

function amp_scripts()
{
    global $wp_query;
    $script_version = '20151216';
    wp_enqueue_style('amp-default-style', get_stylesheet_uri());
    wp_enqueue_style('amp-main-style', get_template_directory_uri() . '/dist/css/styles.min.css');
    wp_enqueue_script('amp-scripts', get_template_directory_uri() . '/dist/js/scripts.min.js', array('jquery'), $script_version, true);

    wp_localize_script('amp-scripts', 'my_ajax_object',
        array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'page' => get_query_var('paged') ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages,
        )
    );

    if (is_front_page()) {
        wp_enqueue_script('amp-home-script', get_template_directory_uri() . '/dist/js/home.min.js', array('jquery'), $script_version, true);
    }
}

add_action('wp_enqueue_scripts', 'amp_scripts');




/* Excluding Folders while migrating with All-in-One WP Migration */
add_filter('ai1wm_exclude_content_from_export', function ($exclude_filters) {
    $exclude_filters[] = 'themes/amplius/node_modules';
    /* $exclude_filters[] = 'updraft'; */
    return $exclude_filters;
});





/*------------------------------------*\
	Example comment heading
\*------------------------------------*/








