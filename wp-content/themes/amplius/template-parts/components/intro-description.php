<?php

$description = get_field('description');

if ($description): ?>
    <div class="c-intro__text">
        <?= $description ?>
    </div>
<?php endif; ?>
