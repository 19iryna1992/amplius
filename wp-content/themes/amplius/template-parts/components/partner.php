<?php
$logo_partner = get_sub_field('logo_partner');
$name_partner = get_sub_field('name_partner');
$link_partner = get_sub_field('link_partner');
$description_partner = get_sub_field('description_partner');

?>

<div class="c-partner"
     data-aos="fade-up"
     data-aos-duration="500"
     data-aos-easing="ease-in-back">

    <div class="row">
        <?php if ($logo_partner): ?>
            <div class="col-12 col-md-5">
                <a href="<?= $link_partner ? $link_partner : ''?>">
                    <div class="c-partner__logo">
                        <img src="<?= $logo_partner['url'] ?>" alt="<?= $logo_partner['alt'] ?>">
                    </div>
                </a>
            </div>
        <?php endif; ?>
        <div class="col-12 col-md-7">
            <div class="c-partner__description">
                <?php if ($name_partner): ?>
                    <h3 class="c-partner__name"><?= $name_partner ?></h3>
                <?php endif; ?>
                <?php if ($description_partner): ?>
                    <div class="c-wysiwyg">
                        <?= $description_partner ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
