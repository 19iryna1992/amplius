<?php

$post_description = get_field('post_description');

?>

<div class="c-post JS--posts--item">
    <a href="<?= get_permalink() ?>">
        <h3 class="c-post__title"> <?= the_title() ?> </h3>

        <?php if ($post_description): ?>

            <div class="c-post__description"><?= $post_description ?></div>

        <?php endif; ?>

        <div>
            <span class="c-button--link"><?php _e('Read More', 'amp'); ?> <span>></span></span>
        </div>
    </a>
</div>


