<?php
$title = get_field('title');
$description = get_field('description');
?>

<?php if ($title || $description): ?>
    <div class="c-intro">

        <?php get_template_part('template-parts/components/intro-title'); ?>

        <?php get_template_part('template-parts/components/intro-description'); ?>

    </div>
<?php endif; ?>
