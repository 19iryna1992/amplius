<?php
// vars
$team_title = get_field('team_title');
$corporate_function = get_field('corporate_function');
$team_contact = get_field('team_contact');
$team_phone = get_field('team_phone');
$team_mail = get_field('team_mail');
$linkedin_link = get_field('linkedin_link');
$description_team = get_field('description_team');
$link_all_teams = get_field('link_all_teams', 'options');

?>

<section class="s-single-team">
    <div class="container"
        data-aos="fade-up"
        data-aos-duration="700">
        <?php if ($link_all_teams): ?>
            <div class="s-single-team__return-btn">
                <a class="c-button--link" href="<?= $link_all_teams ?>">
                    <span><</span> <?php _e('Back To Our Team', 'amp'); ?>
                </a>
            </div>
        <?php endif; ?>
        <div class="s-single-team__wrap">
            <div class="row">
                <div class="col-12 col-md-5">
                    <div class="c-single-team__card">
                        <div class="c-single-team__info d-md-none d-block">
                            <h3 class="c-single-team__title u-blue"><?php the_title(); ?></h3>
                            <?php if ($corporate_function || $team_title): ?>
                                <h4 class="c-single-team__subtitle u-navy">
                                    <?= $team_title ? $team_title : ''?>
                                    <?= $corporate_function ? $corporate_function : '' ?>
                                </h4>
                            <?php endif; ?>
                        </div>
                        <div class="c-single-team__img"
                            data-aos="u-angle-in--right"
                            data-aos-duration="100">
                            <?php the_post_thumbnail($size = 'size_480_343', $attr = '') ?>
                        </div>
                        <div class="c-single-team__contact d-flex">
                            <?php if ($linkedin_link): ?>
                                <div class="c-single-team__linked-in">
                                    <a href="<?= $linkedin_link ?>">
                                        <img src="/wp-content/themes/amplius/img/icons/linked-in.png" alt="">
                                    </a>
                                </div>
                            <?php endif; ?>
                            <div class="c-single-team__contact-info">
                                <?php if ($team_contact): ?>
                                    <div class="c-single-team__name u-blue"><?= $team_contact ?></div>
                                <?php endif; ?>

                                <?php if ($team_phone): ?>
                                    <div class="c-single-team__item u-dark">
                                        <?php _e('phone: ', 'amp'); ?>
                                        <a class="u-dark" href="tel:<?= $team_phone ?>"><?= $team_phone ?></a>
                                    </div>

                                <?php endif; ?>

                                <?php if ($team_mail): ?>
                                    <div class="c-single-team__item u-dark"><?php _e('e-mail: ', 'amp'); ?><a
                                                class="u-dark" href="mailto:<?= $team_mail ?>"><?= $team_mail ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <div class="c-single-team__bio">
                        <div class="c-single-team__info d-md-block d-none">
                            <h3 class="c-single-team__title u-blue"><?php the_title(); ?></h3>
                            <?php if ($corporate_function || $team_title): ?>
                                <h4 class="c-single-team__subtitle u-navy">
                                    <?= $team_title ? $team_title : ''?>
                                    <?= $corporate_function ? $corporate_function : '' ?>
                                </h4>
                            <?php endif; ?>
                        </div>

                        <?php if ($description_team): ?>
                            <div class="c-wysiwyg c-single-team__description">
                                <?= $description_team ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


        <?php if ($link_all_teams): ?>
            <div class="s-single-team__return-btn">
                <a class="c-button--link" href="<?= $link_all_teams ?>">
                    <span><</span> <?php _e('Back To Our Team', 'amp'); ?>
                </a>
            </div>
        <?php endif; ?>

    </div>
</section>
