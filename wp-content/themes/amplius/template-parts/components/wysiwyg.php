<?php
$simple_wysiwyg = get_field('simple_wysiwyg');
?>

<?php if ($simple_wysiwyg): ?>
    <div class="c-wysiwyg">

        <?= $simple_wysiwyg ?>

    </div>
<?php endif; ?>
