<?php
$corporate_function = get_field('corporate_function');
?>

<div class="col-12 col-lg-4 col-sm-6">
    <div class="c-team u-transition-5ms">
        <a href="<?php the_permalink(); ?>">
            <div class="c-team__img">
                <?php the_post_thumbnail($size = 'size_344_246', $attr = '') ?>
            </div>
            <div class="c-team__info">
            <h3 class="c-team__title"><?php the_title(); ?></h3>

            <?php if ($corporate_function): ?>

                <h4 class="c-team__subtitle"><?= $corporate_function ?></h4>

            <?php endif; ?>
            </div>
            
        </a>
    </div>
</div>


