<?php
$title = get_field('title');

if ($title): ?>
    <h2 class="c-intro__title"><?= $title ?></h2>
<?php endif; ?>

