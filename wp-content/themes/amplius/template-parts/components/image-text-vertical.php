<?php
$image = get_field('image');
$text = get_field('text');
?>


<div class="c-image-text-box">
    <?php if ($text): ?>
        <div class="c-wysiwyg">
            <?= $text ?>
        </div>
    <?php endif; ?>
    <?php if ($image): ?>
        <div class="c-image-text-box__image">
            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
        </div>
     <?php endif; ?>
</div>
