<?php
//vars
$company_name = get_field('contact_company_name');
$address_line = get_field('contact_address_line');
$city = get_field('contact_city');
$phone = get_field('contact_phone');
$email = get_field('contact_email');

?>

<div class="c-address d-md-flex justify-content-md-end align-items-md-end"
    data-aos="fade-up"
    data-aos-duration="500"
    data-aos-easing="ease-in-back">
    <div class="c-address__wrap">
        <div class="container">
            <?php if ($company_name): ?>
                <h4 class="c-address__name u-navy">
                    <?= $company_name ?>
                </h4>
            <?php endif; ?>
            <?php if ($address_line): ?>
                <div class="c-address__text u-white">
                    <?= $address_line ?>
                </div>
            <?php endif; ?>
            <?php if ($city): ?>
                <div class="c-address__text u-white">
                    <?= $city ?>
                </div>
            <?php endif; ?>
            <?php if ($phone): ?>
                <div class="c-address__text">
                    <a href="tel:<?= $phone ?>>"><?= $phone ?></a>
                </div>
            <?php endif; ?>

            <?php if ($email): ?>
                <div class="c-address__text">
                    <a href="meilto:<?= $contact_mail ?>"><?= $email ?></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
