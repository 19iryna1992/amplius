<?php
$map = get_field('map');
?>
<?php if ($map): ?>

    <div class="c-map">
        <?= $map ?>
    </div>

<?php endif; ?>
