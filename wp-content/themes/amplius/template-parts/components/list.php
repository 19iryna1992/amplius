<?php if( have_rows('list') ): ?>

    <ul class="c-list">

        <?php while( have_rows('list') ): the_row();

            $item = get_sub_field('item'); ?>

            <?php if( $item ): ?>

            <li class="c-list__item"><?= $item ?></li>

            <?php endif; ?>

        <?php endwhile; ?>

    </ul>

<?php endif; ?>
