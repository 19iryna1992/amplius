<?php
$description_wysiwyg = get_field('description_wysiwyg')
?>

<section class="s-partners">
    <?php if ($description_wysiwyg): ?>
        <div class="container"
            data-aos="fade-up" 
            data-aos-duration="500" 
            data-aos-easing="ease-in-back">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <div class="c-intro">
                        <div class="c-intro__text">
                            <?= $description_wysiwyg ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="s-partners__wrap">
        <div class="container">
            <div class="c-arrow-top"
                data-aos="fade-down"
                data-aos-easing="linear"
                data-aos-duration="700"
                data-aos-offset="100">
                <span></span>
            </div>
            <?php if (have_rows('partners')):

                while (have_rows('partners')) : the_row();

                    get_template_part('template-parts/components/partner');

                endwhile;

            endif ?>
        </div>
    </div>

</section>
