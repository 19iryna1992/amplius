<?php
$title = get_field('title');
$subtitle = get_field('subtitle');

$link_1 = get_field('link_1');
$text = get_field('text');
$link_2 = get_field('link_2');

?>

<?php if (have_rows('hero_slider')): ?>
    <section class="s-home-hero" data-aos="fade"
                                 data-aos-duration="500">
        <div class="swiper-container JS-home-sider">
            <div class="swiper-wrapper">

                <?php while (have_rows('hero_slider')) : the_row();

                    $image = get_sub_field('image');
                    $description = get_sub_field('description');
                    ?>

                    <div class="swiper-slide">
                        <div class="c-home-slide for-anim">
                            <div class="c-home-slide__content-wrap">
                                <div class="c-home-slide__bg"
                                     style="background-image: url('<?= $image ? $image['url'] : '' ?>')"></div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 col-lg-7 col-xl-5">
                                            <div class="c-home-slide__content">
                                                <div class="c-home-slide__main">
                                                    <?php if ($title || $subtitle): ?>

                                                        <div class="c-home-slide__main-top">
                                                            <?php if ($title): ?>
                                                                <h2 class="c-home-slide__title"><?= $title ?></h2>
                                                            <?php endif; ?>

                                                            <?php if ($subtitle): ?>
                                                                <h4 class="c-home-slide__sub-title"><?= $subtitle ?></h4>
                                                            <?php endif; ?>
                                                        </div>

                                                    <?php endif; ?>

                                                    <?php if ($description): ?>
                                                        <div class="c-home-slide__main-body">
                                                            <div class="c-home-slide__text">
                                                                <?= $description ?>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="c-home-slide__content-footer">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-md-8 col-lg-7 col-xl-5">
                                            <div class="c-home-slide__sub">
                                                <div class="c-home-slide__sub-text">
                                                    <?php if ($link_1): ?>
                                                        <a href="<?= $link_1['url'] ?>"
                                                           class="c-home-slide__sub-link"><?= $link_1['title'] ?>
                                                            <span class="c-home-slide__sub-icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 414.496 414.496"><path fill="#008ad8" d="M118.126 0l-28.33 28.238 178.427 179.01-178.427 179.01 28.33 28.238L324.7 207.248z"/></svg>
                                                            </span>
                                                        </a>
                                                        <br>
                                                    <?php endif; ?>

                                                    <?php if ($text): ?>
                                                        <span class="u-navy"><?= $text ?><br></span>
                                                    <?php endif; ?>

                                                    <?php if ($link_2): ?>
                                                        <a href="<?= $link_2['url'] ?>"
                                                           class="c-home-slide__sub-link"><?= $link_2['title'] ?>
                                                            <span class="c-home-slide__sub-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 414.496 414.496"><path fill="#008ad8" d="M118.126 0l-28.33 28.238 178.427 179.01-178.427 179.01 28.33 28.238L324.7 207.248z"/></svg>
                                                            </span>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="s-home-hero__pagination for-anim"></div>
        </div>
    </section>

<?php endif ?>
