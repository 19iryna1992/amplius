<?php if (have_rows('list_section')):
    while (have_rows('list_section')) : the_row();
        //vars
        $icon_section = get_sub_field('icon_section');
        $title_section = get_sub_field('title_section');
        $color_section = get_sub_field('color_section');
        $two_columns_list = get_sub_field('two_columns_list');

        ?>

        <section class="s-list <?= ($two_columns_list) ? 's-list--two-columns' : '' ?>" style="background-color: <?= $color_section  ?>" 
            data-aos="fade-up" 
            data-aos-duration="500" 
            data-aos-easing="ease-in-back">
            <div class="container">
                <div class="row">
                    <?php if ($icon_section): ?>
                        <div class="col-md-2">
                            <div class="s-list__icon">
                                <img src="<?= $icon_section['url'] ?>" alt="<?= $icon_section['alt'] ?>">
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-10 d-md-flex align-items-md-center">
                        <?php if ($title_section): ?>
                            <h2 class="s-list__title u-navy"><?= $title_section ?></h2>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-10 offset-md-2 d-md-flex align-items-md-center">
                        <div class="s-list__list">
                            <?php get_template_part('template-parts/components/list'); ?>
                        </div>
                    </div>
                </div>                
            </div>
        </section>

    <?php endwhile;

endif ?>
