<?php if (have_rows('image_text_horizontal')):
    $count = 0;
    ?>
    <section class="s-our-approach">
        <div class="container">
            <?php while (have_rows('image_text_horizontal')) : the_row();
                $count++ ;
                $title = get_sub_field('title');
                $image = get_sub_field('image');
                $description = get_sub_field('description');
            ?>
                <div class="s-our-approach__wrap"
                    data-aos="fade-up" 
                    data-aos-duration="500" 
                    data-aos-easing="ease-in-back">
                    <div class="row no-gutters">
                        <div class="col-12 d-flex <?= $count % 2 == 0 ? 'flex-lg-row-reverse' : 'flex-lg-row ' ?>  flex-column"
                            data-aos="<?= $count % 2 == 0 ?  'u-angle-in--left' : 'u-angle-in--right' ?>"
                            data-aos-duration="100">
                            <div class="s-our-approach__content">
                                <div class="c-intro">
                                    <?php if ($title): ?>
                                        <h2 class="c-intro__title"><?= $title ?></h2>
                                    <?php endif; ?>

                                    <?php if ($description): ?>
                                        <div class="c-intro__text">
                                            <?= $description ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="s-our-approach__img-wrap"
                            >
                                <div class="s-our-approach__img u-bg-img" style="background-image: url('<?= $image['url']?>')"></div>
                            </div>
                        </div>
                    </div>
                </div>
       <?php endwhile ?>
        </div>
    </section>

<?php endif ?>