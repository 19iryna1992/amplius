<?php
$simple_title = get_field('simple_title');
$simple_text = get_field('simple_text');
?>


<section class="s-about"
    data-aos="fade-up" 
    data-aos-duration="500" 
    data-aos-easing="ease-in-back">
    <div class="container">
        <?php if ($simple_title): ?>
            <div class="row ">
                <div class="col-12">
                    <div class="c-intro">
                        <h2 class="c-intro__title"><?= $simple_title ?></h2>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <?php if ($simple_text): ?>
                <div class="col-12 col-md-6 left-column">
                    <div class="c-wysiwyg">
                        <?= $simple_text ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-12 col-md-6 right-column">

                <?php get_template_part('template-parts/components/image-text-vertical'); ?>

            </div>
        </div>

    </div>
</section>
