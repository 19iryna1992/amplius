<section class="s-our-teams">

         <div class="container"
            data-aos="fade-up" 
            data-aos-duration="700" 
            data-aos-easing="ease-in-back">
            <?php get_template_part('template-parts/components/intro'); ?>
         </div>

        <div class="s-our-teams__wrap"
            data-aos="fade-up" 
            data-aos-duration="500" 
            data-aos-easing="ease-in-back">
            <div class="container">
                <div class="row">

                    <?php
                    $args = array(
                        'post_type' => 'our_team',
                        'posts_per_page' => '-1',
                        'publish_status' => 'published',
                    );
                    $teams = new WP_Query($args); ?>

                    <?php while ($teams->have_posts()) :
                        $teams->the_post(); ?>

                        <?php get_template_part('template-parts/components/our-team'); ?>

                    <?php endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            <div>
        <div>


</section>
