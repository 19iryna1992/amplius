<?php
$wysiwyg = get_field('wysiwyg');
$quote = get_field('quote');

if ($wysiwyg || $quote): ?>
    <section class="s-solutions u-bg-logo">
        <div class="c-bg-logo">
            <svg xmlns="http://www.w3.org/2000/svg" width="760" height="775" viewBox="0 0 760 775">
                <g fill="#F6FBFE" fill-rule="evenodd">
                    <path d="M379.503 164.822l70.067 120.876H309.43l70.073-120.876zm-47.766 241.762l-22.307-38.47h140.14l-22.286 38.444 47.76 82.437L569 326.905 379.503 0 190 326.906 283.961 489l47.776-82.416z"/>
                    <path d="M666.201 164L570.793 164 664.602 326.654 475.4 654.703 380.013 489.307 284.61 654.719 95.398 326.654 189.207 164 93.809 164 0 326.654 258.585 775 310.635 775 380.003 654.729 449.365 775 501.415 775 760 326.648 666.201 164"/>
                </g>
            </svg>
        </div>
        <div class="container" 
            data-aos="fade-up" 
            data-aos-duration="500" 
            data-aos-easing="ease-in-back">
            <div class="row align-items-center">
                <?php if ($wysiwyg): ?>

                    <div class="col-md-6">
                        <div class="c-wysiwyg">
                            <?= $wysiwyg ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($quote): ?>
                    <div class="col-md-6">
                        <div class="c-quote">
                            <?= $quote ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif ?>

