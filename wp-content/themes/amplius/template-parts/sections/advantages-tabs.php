<?php
//vars

$title_tabs_section = get_field('title_tabs_section');
$icon_tabs = get_field('icon_tabs')

?>

<?php if ($icon_tabs): ?>

    <section class="s-advantages">
        <div class="c-bg-logo c-bg-logo--home">
            <svg xmlns="http://www.w3.org/2000/svg" width="760" height="775" viewBox="0 0 760 775">
                <g fill="#F6FBFE" fill-rule="evenodd">
                    <path d="M379.503 164.822l70.067 120.876H309.43l70.073-120.876zm-47.766 241.762l-22.307-38.47h140.14l-22.286 38.444 47.76 82.437L569 326.905 379.503 0 190 326.906 283.961 489l47.776-82.416z"/>
                    <path d="M666.201 164L570.793 164 664.602 326.654 475.4 654.703 380.013 489.307 284.61 654.719 95.398 326.654 189.207 164 93.809 164 0 326.654 258.585 775 310.635 775 380.003 654.729 449.365 775 501.415 775 760 326.648 666.201 164"/>
                </g>
            </svg>
        </div>
        <div class="container"  data-aos="fade-up"
                                data-aos-duration="500"
                                data-aos-easing="ease-in-back">
            <?php if ($title_tabs_section): ?>
                <div class="row justify-content-center">
                    <div class="col-10 col-md-6 col-xl-4">
                        <div class="s-advantages__intro text-center">
                            <h2 class="s-advantages__title"><?= $title_tabs_section ?></h2>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row justify-content-around justify-content-xl-center">
                <div class="col-12">
                    <div class="c-advantage__wrap">
                        <?php while (have_rows('icon_tabs')): the_row();

                            // vars
                            $icon = get_sub_field('icon');
                            $icon_animate = get_sub_field('icon_animate'); //icon for hover
                            $title = get_sub_field('title_tab');
                            $content = get_sub_field('content_tab'); ?>

                            
                                <div class="c-advantage">
                                    <div class="c-advantage__body">
                                        <?php if ($icon): ?>
                                            <div class="c-advantage__icon">
                                                <img src="<?= $icon['url'] ?>" alt="<?= $icon['alt'] ?>">
                                            </div>
                                        <?php endif; ?>

                                        <!--icon for hover-->
                                        <?php if ($icon_animate): ?>
                                            <div class="c-advantage__icon icon--gif d-none">
                                                <img src="<?=$icon_animate?>">
                                            </div>
                                        <?php endif; ?>
                                        <!--icon for hover-->

                                        <?php if ($title): ?>
                                            <h4 class="c-advantage__title"><?= $title ?></h4>
                                        <?php endif; ?>
                                    </div>

                                    <div class="c-advantage__footer">
                                        <?php if ($content): ?>
                                            <div class="c-advantage__content"><?= $content ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
