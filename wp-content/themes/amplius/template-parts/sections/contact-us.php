<?php
$title_section = get_field('title_section');
$description_section = get_field('description_section');
$contact_phone = get_field('contact_phone');
$contact_mail = get_field('contact_mail');

?>

<section class="s-contact-us">

    <div class="c-arrow-top" data-aos="fade-down"
                             data-aos-easing="linear"
                             data-aos-duration="700"
                             data-aos-offset="0">
        <span></span>
    </div>
    <div class="container" data-aos="fade-up" 
                           data-aos-duration="500" 
                           data-aos-easing="ease-in-back">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <?php if ($title_section): ?>                
                    <h2 class="c-intro__title"><?= $title_section ?></h2>
                <?php endif; ?>

                <?php if ($description_section): ?>
                    <p class="c-intro__text"><?= $description_section ?></p>
                <?php endif; ?>


                <div class="s-contact-us__links">
                    <?php if ($contact_phone): ?>
                        <a href="tel:<?= $contact_phone ?>>" class="s-contact-us__link"><?= $contact_phone ?></a>
                    <?php endif; ?>

                    <br>

                    <?php if ($contact_mail): ?>
                        <a href="meilto:<?= $contact_mail ?>" class="s-contact-us__link"><?= $contact_mail ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
