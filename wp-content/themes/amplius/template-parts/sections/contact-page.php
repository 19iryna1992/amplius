<section class="s-contact"
    data-aos="fade-up"
    data-aos-duration="500"
    data-aos-easing="ease-in-back">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php get_template_part('template-parts/components/wysiwyg');?>
            </div>
        </div>
    </div>
</section>
