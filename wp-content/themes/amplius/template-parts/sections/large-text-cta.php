<?php
$image_background = get_field('cta_img_background');
$text = get_field('cta_text');
?>

<section class="s-text-block u-bg-img" style="background-image: url(' <?= $image_background ? $image_background : ''?> ')" 
        data-aos="fade-up" 
        data-aos-duration="500" 
        data-aos-easing="ease-in-back">
    <div class="container">
        <div class="row justify-content-center">
            <?php if( $text ): ?>
            <div class="s-text-block__text u-navy">
                <?= $text ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
