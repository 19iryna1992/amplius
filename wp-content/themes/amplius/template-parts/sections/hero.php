<?php

global $wp_query;

$id = ( isset( $wp_query ) && (bool) $wp_query->is_posts_page ) ? get_option('page_for_posts') : $wp_query->post->ID;

//vars

$image_banner = get_field('image_banner', $id);
$title_banner_white = get_field('title_banner_white', $id);
$title_banner_blue = get_field('title_banner_blue', $id);



if (is_singular('post')):

    $image_banner = get_field('post_image_banner', 'option');
    $title_banner_white = get_field('post_title_banner_white', 'option');
    $title_banner_blue = get_field('post_title_banner_blue', 'option');


elseif (is_singular('our_team')):
    $image_banner = get_field('team_image_banner', 'option');
    $title_banner_white = get_field('team_banner_white', 'option');
    $title_banner_blue = get_field('team_title_banner_blue', 'option');


endif ?>

<section class="s-hero">
    <div class="s-hero__bg u-bg-img" style="background-image: url('<?= $image_banner ? $image_banner['url'] : '' ?>')" 
    data-aos="fade"
    data-aos-duration="500"></div>

    <div class="s-hero__label__wrap">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-hero__label for-anim">
                        <?php if ($title_banner_white || $title_banner_blue): ?>
                            <h1 class="s-hero__title">
                                <?php if ($title_banner_white): ?>
                                    <span class="u-white"><?= $title_banner_white ?></span>
                                <?php endif; ?>

                                <?php if ($title_banner_blue): ?>
                                    <?= $title_banner_blue ?>
                                <?php endif; ?>
                            </h1>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>

