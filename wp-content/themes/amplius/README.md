# Starter Theme



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Installing


Clone repository

```
git clone https://gitlab.com/Andriy_007/starter-theme.git
```

In theme folder exist ".gitignore" file

```
Move this file in main folder
```

Install npm dependencies in client theme folder

```
npm install
```

Run the gulp command

```
gulp
```

## Wordpress

Make sure you are replaced all domain prefixes
```
this theme use  "esr"
```


## Support

Andriy Omelyanchuk
