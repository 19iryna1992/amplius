<?php

get_header(); ?>

<main id="main" role="main" tabindex="-1">
    <?php while (have_posts()) :
        the_post(); ?>
        <?php get_template_part('template-parts/sections/hero'); ?>
        <?php get_template_part('template-parts/components/single-team'); ?>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

</main>

<?php get_footer(); ?>
