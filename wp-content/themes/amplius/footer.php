<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package amp
 */
$logo = get_field('logo_footer', 'option');
$copyright = get_field('copyright', 'option');
$privacy_policy = get_field('privacy_policy', 'option');
$disclosures = get_field('disclosures', 'option');

?>

</div><!-- #content -->

<footer class="o-footer">
    <div class="container">
    <div class="c-back-btn d-flex justify-content-end ">
        <a class="c-button--link" href="#">
            <span><</span> Back To Top
        </a>
    </div>
    </div>
    <div class="c-footer__top">
        <div class="container">
            <div class="row align-items-center">
                <?php if ($logo): ?>

                    <div class="col-12 col-md-3">
                        <div class="c-footer__logo"  
                            data-aos="fade"
                            data-aos-easing="linear"
                            data-aos-duration="500"
                            data-aos-offset="0">
                            <img src="<?= $logo['url'] ?>" alt="<?= $logo['alt'] ?>">
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-12 col-md-9"
                    data-aos="fade"
                    data-aos-easing="linear"
                    data-aos-duration="500"
                    data-aos-offset="0">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-top-menu',
                        'menu_class' => 'c-footer-menu__list',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="c-footer__bottom">
        <div class="container">
            <div class="row"
                data-aos="fade"
                data-aos-easing="linear"
                data-aos-duration="500"
                data-aos-offset="0"
                data-aos-delay="50">
                <div class="col-12 d-md-flex align-items-center justify-content-center">
                    <div class="c-footer__copyright">
                        <?php if ($copyright): ?>
                            <p><?= $copyright ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="c-footer__menu c-footer__menu--bottom">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'footer-bottom-menu',
                            'menu_class' => 'c-footer-menu__list',
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
