<?php

function our_team_post_type()
{
    $labels = array(
        'name' => _x('Teams', 'Post Type General Name', 'amp'),
        'singular_name' => _x('Team', 'Post Type Singular Name', 'amp'),
        'menu_name' => __('Teams', 'amp'),
        'parent_item_colon' => __('Parent ', 'amp'),
        'all_items' => __('All Teams', 'amp'),
        'view_item' => __('View Team', 'amp'),
        'add_new_item' => __('Add New Team', 'amp'),
        'add_new' => __('Add New', 'amp'),
        'edit_item' => __('Edit Team', 'amp'),
        'update_item' => __('Update Team', 'amp'),
        'search_items' => __('Search Teams', 'amp'),
        'not_found' => __('Not Found', 'amp'),
        'not_found_in_trash' => __('Not found in Trash', 'amp'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label' => __('Teams', 'amp'),
        'description' => __('Our Teams', 'amp'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail','revisions'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_icon'     => 'dashicons-businessperson',
        'menu_position' => 3,
        'rewrite' => array('slug' => 'team', 'with_front' => false),
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true
    );

    // Registering your Custom Post Type
    register_post_type('our_team', $args);

}


add_action('init', 'our_team_post_type');

