(function ($) {


    $(document).ready(function () {

      var swiper = new Swiper('.JS-home-sider', {
				spaceBetween: 0,
				effect: 'fade',
				speed: 0,
				allowTouchMove: false,
				loop: true,
				pagination: {
					el: '.JS-home-sider .s-home-hero__pagination',
					clickable: true,
				},
				autoplay: {
					delay: 3000,
				},

				on: {
					init: function () {
						
						setTimeout(function(){
							$('.c-home-slide').each(function(){
								$(this).removeClass('for-anim');
							});
							$('.s-home-hero__pagination').removeClass('for-anim');
						}, 100);

						$('.swiper-slide').removeClass('anim');
					},
					slideChangeTransitionStart: function () {
						$('.swiper-slide-active').addClass('anim');

						setTimeout(function(){
							$('.swiper-slide').removeClass('anim');
						}, 600);
						
					},
				},
			});

			$(".c-advantage").each(function () {
					var $advantage = $(this);

					$advantage.hover(
						function() {
							$(this).find('.c-advantage__icon').not('.icon--gif').addClass("d-none");
							$(this).find('.icon--gif').removeClass("d-none");
						},
						function() {
							$(this).find('.c-advantage__icon').not('.icon--gif').removeClass("d-none");
							$(this).find('.icon--gif').addClass("d-none");
						})

				}
			);

    });


})(jQuery);



